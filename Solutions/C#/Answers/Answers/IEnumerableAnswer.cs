﻿using System;
using System.Collections;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Answers
{
    [TestClass]
    public class UnitTestIEnumerable
    {
        [TestMethod]
        public void TestMethod1()
        {
            string[] colors = { "Red", "Yellow", "Pink", "Blue" };
            int i = 0;
            var exampleCollection = new Colors();
            foreach (var item in exampleCollection)
            {
                System.Console.WriteLine(item);
                Assert.AreEqual(colors[i++], item);
            }
        }

     }

   
    public class Colors : IEnumerable, IEnumerator
    {
        string[] colors = { "Red", "Yellow", "Pink", "Blue" };
        int position = -1;
            
        public bool MoveNext()
        {
            position++;
            return (position < colors.Length);
        }

        public void Reset()
        {
            position = -1;
        }

        object IEnumerator.Current
        {
            get { return colors[position]; }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return new Colors();
        }
    
    }
    
}
