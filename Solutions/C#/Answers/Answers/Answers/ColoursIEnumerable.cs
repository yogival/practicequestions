﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Answers.UnitTests
{
    class ColoursIEnumerable : IEnumerable
    {
        private string[] p;

        public ColoursIEnumerable(string[] p)
        {
            // TODO: Complete member initialization
            this.p = p;
        }

        public IEnumerator GetEnumerator()
        {
            return p.GetEnumerator(); // Arrays have method called GetEnumerator
        }
    }
}
