﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Answers.UnitTests
{
    class GenericClass<T> : IEnumerable<T>
    {
        private T[] p;
        int position = -1;
        public GenericClass(T[] p)
        {
            this.p = p;
        }

        public IEnumerator<T> GetEnumerator()
        {
            foreach (var item in p)
            {
                yield return item;
            }
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return p.GetEnumerator();
        }
    }
}
