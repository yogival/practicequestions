﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Answers.UnitTests
{
    /**
     * Demonstrate the IEnumberator class
     */
    [TestClass]
    public class GenericEnumeratorTest
    {
        private int StandardImplementation(string colour, string[] colours)
        {
            int i = -1;
            while (++i < colours.Length)
            {
                if (colours.Equals(colours[i]))
                    return i;
            }
            return -1;
        }

        private int StandardImplementation(int colors, int[] colours)
        {
            int i = -1;
            while (++i < colours.Length)
            {
                if (colours.Equals(colours[i]))
                    return i;
            }
            return -1;
        }

        /**
         * Complete the implementation of evaluate
         */
        [TestMethod]
        public void TestGenericEnumerator_string()
        {
            string[] color = { "Red", "Yellow", "Orange" };

            int expected = StandardImplementation("Orange", color);

            string actual = Evaluate<string>("Orange", color.GetEnumerator());
                       
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestGenericEnumerator_int()
        {
            int[] color = { 6,1,4 };

            int expected = StandardImplementation(6, color);

            int actual = Evaluate<int>(6, color.GetEnumerator());

            Assert.AreEqual(expected, actual);
        }

        private T Evaluate<T>(T colour, System.Collections.IEnumerator enumerator) where T : IComparable<T>
        {
            while (enumerator.MoveNext())
            {
                if (enumerator.Current.Equals(colour))
                    return (T)enumerator.Current;
            }
            return default(T);
        }

    }
}
