﻿using System;
using System.Collections;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Answers.UnitTests
{
    /**
     * Question on IEnumerator: task complete the implementation of Evaluate to make the 
     * test TestEnumerator pass
     */
    [TestClass]
    public class EnumeratorTest
    {
        /**
         * Complete the implementation of evaluate
         */
        [TestMethod]
        public void TestEnumerator()
        {
            string[] color = { "Red", "Yellow", "Orange" };

            string expected =StandardImplementation(color); 
            
            IEnumerator enumerator = color.GetEnumerator();         
            string actual = Evaluate(enumerator);
                       
            Assert.AreEqual(expected, actual);
        }

        private string Evaluate(IEnumerator enumerator)
        {
            throw new NotImplementedException();
        }

        private string StandardImplementation(string[] color)
        {

            int i = 0;
            string output = "";
            while (i < color.Length)
            {
                output += color[i];
                i++;
            }
            return output;
        }

    }
}
