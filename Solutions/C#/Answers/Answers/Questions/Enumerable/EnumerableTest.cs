﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Answers.UnitTests;

namespace Answers
{
    [TestClass]
    public class EnumerableUnitTest
    {
        private string StandardImplementation(string[] colour)
        {
            string output = "";
            for(int i = 0; i < colour.Length; i++)
                output += colour[i];
            return output;
        }


        [TestMethod]
        public void TestForEachImplementation()
        {
            string[] colourArray= { "Red", "Yellow", "Orange" };
            ColoursIEnumerable colour = new ColoursIEnumerable(colourArray);
            string output = "";
            foreach (string c in colour)
               output += c;

            string expected = StandardImplementation(colourArray);
            Assert.AreEqual(expected, output);
        }
    }
}
