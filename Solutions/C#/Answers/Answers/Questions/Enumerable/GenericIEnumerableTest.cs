﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Answers.UnitTests;

namespace Answers
{
    [TestClass]
    public class GenericIEnumerableTest
    {

        [TestMethod]
        public void TestForEachCarsImplementation()
        {

            GenericClass<string> cars = new GenericClass<string>(new string[]{"Ferrari ","Porshe ", "Lambo"}); // Class containing a list of car names
            string output = "";
            foreach (string c in cars)
            {
                output += c;            
            }
            Assert.AreEqual("Ferrari Porshe Lambo", output);
        }

        [TestMethod]
        public void TestForEachNumbersImplementation()
        {
            GenericClass<int> nums = new GenericClass<int>(new int[] { 6, 1, 4 }); // Class containing a list of numbers
            int output = 0;
            foreach (int n in nums)
            {
                output += n;
            }
            Assert.AreEqual(11, output);
        }
    }

}
