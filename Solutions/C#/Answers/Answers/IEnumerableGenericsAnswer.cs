﻿using System;
using System.Collections;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Answers
{
    [TestClass]
    public class IEnumerableGenericsAnswer
    {
        [TestMethod]
        public void TestMethod1()
        {
            string[] colors = { "Red", "Yellow", "Pink", "Blue" };
            int i = 0;
            var exampleCollection = new MyGenericCollection<string>(colors);
            foreach (var item in exampleCollection)
            {
                System.Console.WriteLine(item);
                Assert.AreEqual(colors[i++], item);
            }
 
        }

        [TestMethod]
        public void TestMethod2()
        {
            int[] keys = { 6, 1, 4 };
            int i = 0;
            var exampleCollection = new MyGenericCollection<int>(keys);
            foreach (var item in exampleCollection)
            {
                System.Console.WriteLine(item);
                Assert.AreEqual(keys[i++], item);
            }

        }
    }
    
    public class MyGenericCollection<T> : IEnumerable<T>, IEnumerator<T>
    {
        IList<T> collection;
        int position = -1;
        public MyGenericCollection(IList<T> collection)
        {
            this.collection = collection;
        }

        public IEnumerator<T> GetEnumerator()
        {
            return collection.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return collection.GetEnumerator();    
        }

        
        object IEnumerator.Current
        {
            get {return collection[position]; }
        }

        public bool MoveNext()
        {
            position++;
            return (position < collection.Count);
        }

        public void Reset()
        {
            position = -1;
        }

        T IEnumerator<T>.Current
        {
            get { throw new NotImplementedException(); }
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
